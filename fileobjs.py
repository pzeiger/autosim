class FileList:
    """Simple list of file objects
    """
    
    def __init__(self, **kwargs):
        
        self.filelist = []
        keys = kwargs.get('key', None)
        fileobjs = kwargs.get('fileobj', None)
        if (keys is not None) and (fileobjs is not None):
            assert isinstance(keys, list)
            assert isinstance(fileobjs, list)
            for key,fileobj in zip(keys, fileobjs):
                self.append_file(key, fileobj)
    
    
    def append_file(self, fileobj):
        """
        """
        assert isinstance(fileobj, FileObj), \
            'fileobj not instance of FileObj'
        self.filelist.append(fileobj)
    
    
    def as_array(self):
        """
        """
        return self.filelist



class FileObj:
    """
    """
    
    def __init__(self, name, path, action):
        self.name = name
        self.path = path 
        self.action = action
    
    
    def __repr__(self):
        return '({}, {})'.format(self.path, self.action)
    
    
    def as_array(self):
        return [self.path, self.action]
    


class SimpleDB:
    """
    """
    
    def __init__(self, key, entry):
        
        self.append(self, key, entry)
    
    
    def append_(self, key, entry):
        """
        """
        setattr(self, key, entry)


