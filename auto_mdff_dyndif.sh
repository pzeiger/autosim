#!/bin/bash

thetain="theta.in"
maxwgevin="maxwgev.in"
maxwgabsin="maxwgabs.in"
minbwcin="minbwc.in"
inseltpl="Fe.insel2_tpl"
expansionin="expansion.in"
tmpfile="tmp"
nthreads=4
idlethread=(0 1 2 3)
parentwd=$(pwd)


# Read outgoing beam angles
i=0
while read -r line
do
	theta[$i]=${line//[[:blank:]]/}
	i="$((i+1))"
done < "$thetain"

# Read maximum excitation error for EV problem
i=0
while read -r line
do
	maxwgev[$i]=${line//[[:blank:]]/}
	i="$((i+1))"
done < "$maxwgevin"

# Read maximum excitation error for beam selection
i=0
while read -r line
do
	maxwgabs[$i]=${line//[[:blank:]]/}
	i="$((i+1))"
done < "$maxwgabsin"

# Read minimum bloch wave coefficient excitation
i=0
while read -r line
do
	minbwc[$i]=${line//[[:blank:]]/}
	i="$((i+1))"
done < "$minbwcin"

# Read minimum bloch wave coefficient excitation
i=0
while read -r line
do
	expansion[$i]=${line//[[:blank:]]/}
	i="$((i+1))"
done < "$expansionin"

# Prepare multithreading
for (( i=0; i<nthreads; i++ ))
do
	mkdir -p thread_$i
done


currentthread=${idlethread[0]}
idlethread[0]=''

# run mdff and dyndif
for tx in "${theta[@]}"
do
	for ty in "${theta[@]}"
	do
		echo $ty'>='0 | bc -l
		echo $tx $ty
		if [ $(echo $ty'>='0 | bc -l) -eq 0 ]; then
			echo true
		else
			for mev in "${maxwgev[@]}"
			do
				for mabs in "${maxwgabs[@]}"
				do
					for mbwc in "${minbwc[@]}"
					do
						for exp in "${expansion[@]}"
						do
							cp Fe.* thread_$currentthread/
							cp *.def thread_$currentthread/
							cp -a run_mdff_dyndif.sh thread_$currentthread/
							
							prefix1="theta_"${tx}"_"${ty}
							prefix2=$mev"_"$mabs"_"$mbwc"_"$exp
							
							echo ${prefix1}_${prefix2} start
							mkdir -p $prefix1
							mkdir -p $prefix1/$prefix2
							
							sed 's/<THETAX>/'$tx'/g' $inseltpl > ${prefix1}_${prefix2}.insel2
							sed -i 's/<THETAY>/'$ty'/g' ${prefix1}_${prefix2}.insel2
							sed -i 's/<MAXWGEV>/'$mev'/g' ${prefix1}_${prefix2}.insel2
							sed -i 's/<MAXWGABS>/'$mabs'/g' ${prefix1}_${prefix2}.insel2
							sed -i 's/<MINBWC>/'$mbwc'/g' ${prefix1}_${prefix2}.insel2
							sed -i 's/<EXPANSION>/'$exp'/g' ${prefix1}_${prefix2}.insel2
							
							mv ${prefix1}_${prefix2}.insel2 thread_$currentthread/Fe.insel2
							cd thread_$currentthread
							rm Fe.mdff
							ls
							nohup ./run_mdff_dyndif.sh  > $parentwd/$prefix1/$prefix2/${prefix1}_${prefix2}.log && mv ./* $parentwd/$prefix1/$prefix2/ && ${idlethread[@]} $(pwd | egrep -o 'thread_[0-9]*' | cut -f 2 -d '_') &
							cd $parentwd
							echo ${prefix1}_${prefix2} done!
							
							
							sleep 5s
							
							while [ ${#idlethread[@]} -eq 0 ]
							do
								echo All threads running. Waiting...
								sleep 10s
							done
							
							currentthread=${idlethread[-1]}
							idlethread[-1]=''
							echo $currentthread
						done
					done
				done
			done
		fi
	done
done


