import sys


class ArgObj:
    """
    """
    
    def __init__(self, key='', value=[]):
        if isinstance(key, list):
            key = key[0]
        self.key = key
        self.value = value
    
    def __repr__(self):
        return repr((self.key, self.value))


class CommandList:
    """
    """
    
    def __init__(self, **kwargs):
        
        keys = kwargs.get('key', None)
        cmdobjs = kwargs.get('cmdobj', None)
        order = kwargs.get('order', None)
        
        if (keys is not None) and (cmdobjs is not None):
            assert isinstance(keys, list)
            assert isinstance(cmdobjs, list)
            for key,cmdobj in zip(keys, cmdobjs):
                self.append_cmd(key, cmdobj)
        
        self.order = order
    
    
#    def __repr__(self):
#        return repr((self.order, self.key))

    
    def append_cmd(self, key, cmdobj):
        """
        """
        assert isinstance(cmdobj, CommandObj), \
            'cmdobj not instance of CommandObj'
        setattr(self,key,cmdobj)
    
    def set_order(self, order):
        """
        """
        self.order = order
    

    def get_order(self):
        """
        """
        return self.order





class CommandObj:
    """
    """
    
    def __init__(self, exe='', args=[], posargs=[], flag=''):
        self.exe = exe
        self.args = []
        self.posargs = []
        self.flag = flag
        
        for arg in args:
            arg_obj = ArgObj(arg[0], arg[1])
            self.args.append(arg_obj)
        for posarg in posargs:
            posarg_obj = ArgObj(posarg[0], posarg[1])
            self.posargs.append(posarg_obj)
    
    
    def __repr__(self):
        return '{},[{}, {}]'.format(self.exe, self.args, self.posargs)
    
    
    def as_str(self):
        """
        """
        return ' '.join(as_list())


    def as_list(self):
        """
        """
        self.posargs.sort(key=lambda posarg: posarg.key)
        self.args.sort(key=lambda arg: arg.key)
        
        cmd = [self.exe]
        print(cmd)
        for arg in self.args:
            print(cmd, arg)
            cmd = cmd + [arg.key] + arg.value
        
        for posarg in self.posargs:
            print(cmd, posarg)
            cmd = cmd + posarg.value
        
        return cmd


