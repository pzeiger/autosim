import numpy as np
import os
import sys
import pathlib
import shutil
from fileobjs import FileList, FileObj
from cmdobjs import CommandList, CommandObj
from contextlib import contextmanager


if sys.version_info >= (3, 5):
    from subprocess import run as sp_run
else:
    from subprocess import call as sp_run


@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)


class SciSim:
    """Object to store and settings for scientific simulations.
    """
    
    
    def __init__(self, *non_default_settings):
        # External commands to run simulation
        self.exec_cmd = CommandList()
        self.exec_stdout = 'scisim.stdout'
        self.exec_stderr = 'scisim.stderr'
        
        # File names and basedir
        self.file_prefix = ''
        self.basedir = os.getcwd()
        self.rundir = ''
        self.file_list = FileList()
    
    
    def attributes(self):
        """Produce list of attributes of class excluding magic methods.
        """
        return [it for it in self.__dict__.keys()
            if not it.startswith("__")
        ]
    
    
    def write_file(self, path):
        """
        """
        raise NotImplementedError('SciSim: method write_input not implemented')
    
    
    def resolve_path_input_filenames(self, filename):
        """
        """
        if filename[0] == '.':
            return self.resolve_path_input_filenames(filename[1:])
        elif filename[0] == '/':
            return self.resolve_path_input_filenames(filename[1:])
        else:
            return self.basedir + '/' + filename

    
    def handle_files(self):
        """
        """
        for fi in self.file_list.as_array():
            if getattr(fi,'action') == "cpy":
                shutil.copy(getattr(fi,'path'),self.rundir)
            elif getattr(fi,'action') == "ln":
                src = self.resolve_path_input_filenames(getattr(fi,'path'))
                dst = self.rundir + '/' + getattr(fi,'name')
                os.symlink(src, dst)
            elif getattr(fi, 'action') == "gen":
                self.write_file(getattr(fi,'path'))
            else:
                raise NotImplementedError('file action %s not implemented' \
                    % getattr(fi,'action'))
    
    
    def set_rundir(self, rundir):
        """
        """
        self.rundir = self.basedir + '/' + rundir
        
        
    def create_dir(self, dirname):
        """
        """
        print('creating directory %s' % dirname)
        try:
            pathlib.Path(dirname).mkdir()
        except FileExistsError:
            print('FileExistsError: directory %s exists' % dirname)
            answer = input('Do you want to continue? Y/y/n/N:')
            if answer not in ['y', 'Y']:
                sys.exit()
   

    def run(self):
        """
        """
        self.create_dir(self.rundir)
        self.handle_files()
        
        file_stdout = self.rundir + '/' + self.exec_stdout
        file_stdout = open(file_stdout, 'w')
        file_stderr = self.rundir + '/' + self.exec_stderr
        file_stderr = open(file_stderr, 'w')
        
        for key in self.exec_cmd.get_order():
            if getattr(self.exec_cmd, key).flag == 'cpy':
                shutil.copy(getattr(self.exec_cmd, key).exe,self.rundir)
                getattr(self.exec_cmd, key).flag = 'copied'
            elif getattr(self.exec_cmd, key).flag == 'ln':
                src = self.resolve_path_input_filenames(
                    getattr(self.exec_cmd, key).exe
                )
                dst = self.rundir + '/' + getattr(self.exec_cmd, key).exe
                os.symlink(src, dst)
                getattr(self.exec_cmd, key).flag = 'linked'

        with cd(self.rundir):
        
            for key in self.exec_cmd.get_order():
                cmd = getattr(self.exec_cmd, key).as_list()
#                print(cmd)
#                sp_run(['ls'])
                status = sp_run(cmd, stderr=file_stderr, stdout=file_stdout)
#                print(status)
            
            for key in self.exec_cmd.get_order():
                if getattr(self.exec_cmd, key).flag == 'copied':
                    pathlib.Path(getattr(self.exec_cmd, key).exe).unlink()
                    getattr(self.exec_cmd, key).flag = 'removed'
        
    
    def write_att_1p10f(self, filehandle, att_list):
        """
        """
        out = ''
        atts = self.return_list_atts(att_list)
        for att in atts:
            if isinstance(att, list):
                for ele in att:
                    out = out + '%1.10f' % float(ele) + '  '
            else:
                out = out + '%1.10f' % float(att)
        filehandle.write('%s' % out+'\n')
    
    
    def write_att_float(self, filehandle, att_list):
        """
        """
        out = ''
        atts = self.return_list_atts(att_list)
        for att in atts:
            if isinstance(att, list):
                for ele in att:
                    out = out + '%f' % float(ele) + '  '
            else:
                 out = out + '%f' % float(att)
        filehandle.write('%s' % out+'\n')
    
    
    def write_att_int(self, filehandle, att_list):
        """
        """
        out = ''
        atts = self.return_list_atts(att_list)
        for att in atts:
            if isinstance(att, list):
                for ele in att:
                    out = out + '%i' % int(ele) + '  '
            else:
                out = out + '%i' % int(att)
        filehandle.write('%s' % out+'\n')
    
    
    def write_att_str(self, filehandle, att_list):
        """
        """
        out = ''
        atts = self.return_list_atts(att_list)
        for att in atts:
            if isinstance(att, list):
                for ele in att:
                    out = out + '%s' % str(ele) + '  '
            else:
                out = out + '%s' % str(att)
        filehandle.write('%s' % out+'\n')
    
    
    def return_list_atts(self, attributes):
        """
        """
        if isinstance(attributes,list):
            atts = [getattr(self, a) for a in attributes]
        else:
            atts = [getattr(self, attributes)]
        return atts


