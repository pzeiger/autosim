import numpy as np
import os
import sys
import simmods.scisim as scisim
import pathlib


class SimDef(scisim.SciSim):
    """Object to store and manipulate settings for simulations of
    inelastic scattering in the transmission electron microscope.
    
    """
    
    def __init__(self, *non_default_settings):
        
        print('initializing DyndifMdffSim')
        # call init of parent class
        scisim.SciSim.__init__(self)
        
#       # External script that runs simulation
#       self.exec_cmd = []
#       self.exec_stdout = 'run_mdff_dyndif.stdout'
#       self.exec_stderr = 'run_mdff_dyndif.stderr'
        self.exec_version = 'mod_pz'
#       
        # File names and workdir
        self.file_prefix = 'Fe'
        self.file_insel2 = self.file_prefix + '.insel2'
        
        # ATOM PROPERTIES
        #
        # 1 atom index, nc, lc of the initial state
        # 2 energy loss 
        # 3 edge distance (eV)
        self.atom_init_state = [1, 2, 1]  #1
        self.atom_energy_loss = 708  # 2
        self.atom_edge_dist = 13  # 3
        
        # INCOMING BEAM PROPERTIES
        #
        # 1 beam energy (keV)
        # 2 zone axis, or for general orientations, k-vector
        # 3 x-dir
        # 4 incoming beam angle input mode LCC/THETA
        # 5 Laue circle center
        self.in_beam_energy = 200  # 1
        self.in_k_vec = [0, 1, 6]  # 2
        self.in_xdir = [1, 0, 0]  # 3
        self.in_angle_mode = 'THETA'  # 4
        self.in_laue_center = [0.00, 0.00]  # 5
        
        # OUTGOING BEAM PROPERTIES
        #
        # 1 outgoing beam direction defined w.r.t. zone axis (RELZA) or
        #   transmitted beam (RELTB)
        # 2 mode of input of outgoing beam: THETA (mrads) or MULG (in fractional
        #   multiples of G given below)
        # 3 theta_x w.r.t. incoming beam, x || hkl projection to plane perp.
        #   to chi_in
        # 4 theta_y
        self.out_dir_def = 'RELTB'  # 1
        self.out_angle_mode = 'THETA'  # 2
        self.out_angle_x = -24.0  # 3
        self.out_angle_y = 0.0  # 4
        
        # POTENTIAL PROPERTIES
        # 
        # 1 V_hkl calculation/type: DOYLE, WEICK, WIEN
        # 2 absorptive part of potential: WEICK, COEFF
        # 3 if absorptive part of potential is 'COEFF', then 
        #   V_abs(hkl) = i*coeff*V(hkl)
        self.pot_v_hkl_calc_type = 'WEICK'  # 1
        self.pot_absorp_mode = 'COEFF'  # 2
        self.pot_absorp_coeff  = 0.00  # 3
        
        # BEAM SELECTION FOR EIGENVALUE PROBLEM
        # 
        # 1 mode: AUTO for automatic selection, FILE for reading them from
        #   fort.18
        # 2 max h,k,l indices for automatic beam generation
        # 3 G_max for automatic selection of beams (in a.u.^-1)
        # 4 maximum wg => max. excitation error x extinction distance
        #   (dimensionless)
        self.evp_beam_select_mode = 'AUTO'  # 1
        self.evp_beam_select_max_ind = [39, 39, 39]  # 2
        self.evp_beam_select_gmax = 100.1  # 3
        self.evp_beam_select_max_wg = 1000  # 4
        
        # BEAM/BLOCHS SELECTION FOR SUMMATION
        #
        # 1 mode: AUTO for automatic selection, FILE for reading them from
        #   fort.17
        # 2 if 'AUTO', then this is max wg for automatic beam selection
        #   (dimensionless)
        # 3 minimal Bloch-wave coefficient excitation C_0
        self.sum_beam_select_mode = 'AUTO'  # 1
        self.sum_max_wg = 1000  # 2
        self.sum_min_bloch_wave_coeff = 0.1  # 3
        
        # ENERGY MESH SETTINGS
        #
        # 1 Fermi level (in Ry)
        # 2 E_min, E_step, E_steps (in eV) with respect to E_F (Fermi level)
        # 3 treated energy range (eV)
        self.energy_fermi_level = '0.6067337934'  # 1
        self.energy_input = [-130.552139, 0.136058, 1116]  # 2
        self.energy_range = [0, 10.0]  # 3

        # SAMPLE SETTINGS
        #
        # sample thickness (nm): t_min, t_step, nsteps
        self.sample_thickness = [1.0, 0.5, 99]
        
        # MDFF
        # 1 dos lmax
        # 2 final state; finl1min, finl1max, finl2min, finl2max <= dos lmax
        # 3 Bessel expansion; besl1min, besl1max, besl2min, besl2max 
        # 4 DIPOLE, LAMB1 or FULL; if DIPOLE, Bessel expansion is irrelevant
        # 5 SUM or DETAIL, if DETAIL all different terms of the Bessel 
        #   expansion are output
        # 6 REL or NOREL - for future implementation of relativistic mdff
        # 7 magnetization direction (usually same as zone axis)
        self.mdff_dos_lmax = 3  # 1
        self.mdff_l_final_state = [0, 3, 0, 3]  # 2
        self.mdff_l_bessel_exp = [0, 3, 0, 3]  # 3
        self.mdff_bessel_exp_approx = 'FULL'  # 4
        self.mdff_bessel_exp_output = 'SUM'  # 5
        self.mdff_rel_mode = 'NOREL'  # 6
        self.mdff_magn_dir = [0, 0, 1]  # 7
    
        
        # overwrite default settings
        for setting in non_default_settings:
            assert setting in self.attributes, 'Setting not understood.'
            self.setting[0] = self.setting[1]
   

    
    def attributes(self):
        """Produce list of attributes of class excluding magic methods.
        """
        return [it for it in self.__dict__.keys()
            if not it.startswith("__")
        ]
        
    def write_file(self, path):
        """ Redefinition of parent class method write_input()
        """
        if pathlib.Path(path).parts[-1] == self.file_insel2:
            self.write_insel2()
        else:
            raise NotImplemented('No routine for writing file %s' % path)
    
    def write_insel2(self):
        """Write out insel file
        """
        file_handle_insel2 = open(self.rundir + '/' + self.file_insel2, 'w')
        
        file_handle_insel2.write('# ATOM\n')
        self.write_att_int(file_handle_insel2, 'atom_init_state')
        self.write_att_int(file_handle_insel2, 
            ['atom_energy_loss', 'atom_edge_dist'],
        )
        
        file_handle_insel2.write('\n# INCOMING BEAM\n')
        self.write_att_int(file_handle_insel2, 'in_beam_energy')
        self.write_att_int(file_handle_insel2, 'in_k_vec')
        self.write_att_int(file_handle_insel2, 'in_xdir')
        self.write_att_str(file_handle_insel2, 'in_angle_mode')
        self.write_att_float(file_handle_insel2, 'in_laue_center')
        
        file_handle_insel2.write('\n# OUTGOING BEAM\n')
        self.write_att_str(file_handle_insel2, 'out_dir_def')
        self.write_att_str(file_handle_insel2, 'out_angle_mode')
        self.write_att_float(file_handle_insel2,
            ['out_angle_x', 'out_angle_y'],
        )
        
        file_handle_insel2.write('\n# POTENTIAL OPTIONS\n')
        self.write_att_str(file_handle_insel2, 'pot_v_hkl_calc_type')
        self.write_att_str(file_handle_insel2, 'pot_absorp_mode')
        self.write_att_float(file_handle_insel2, 'pot_absorp_coeff')
        
        file_handle_insel2.write('\n# BEAM SELECTION FOR EIGENVALUE PROBLEM\n')
        self.write_att_str(file_handle_insel2, 'evp_beam_select_mode')
        self.write_att_int(file_handle_insel2, 'evp_beam_select_max_ind')
        self.write_att_float(file_handle_insel2, 'evp_beam_select_gmax')
        self.write_att_int(file_handle_insel2, 'evp_beam_select_max_wg')
        
        file_handle_insel2.write('\n# BEAM/BLOCH SELECTION FOR SUMMATION\n')
        self.write_att_str(file_handle_insel2, 'sum_beam_select_mode')
        self.write_att_int(file_handle_insel2, 'sum_max_wg')
        self.write_att_float(file_handle_insel2, 'sum_min_bloch_wave_coeff')
        
        file_handle_insel2.write('\n# ENERGY MESH\n')
        self.write_att_1p10f(file_handle_insel2, 'energy_fermi_level')
        self.write_att_1p10f(file_handle_insel2, 'energy_input')
        self.write_att_1p10f(file_handle_insel2, 'energy_range')
        
        file_handle_insel2.write('\n')
        self.write_att_float(file_handle_insel2, 'sample_thickness')
        
        file_handle_insel2.write('\n# MDFF\n')
        self.write_att_int(file_handle_insel2, 'mdff_dos_lmax')
        self.write_att_int(file_handle_insel2, 'mdff_l_final_state')
        self.write_att_int(file_handle_insel2, 'mdff_l_bessel_exp')
        self.write_att_str(file_handle_insel2, 'mdff_bessel_exp_approx')
        
        #Activate Bessel output modifications
        if self.exec_version[0] == 'modpz':
            self.write_att_str(file_handle_insel2, 'mdff_bessel_exp_output')
        
        self.write_att_str(file_handle_insel2, 'mdff_rel_mode')
        self.write_att_float(file_handle_insel2, 'mdff_magn_dir')
    
    
 
