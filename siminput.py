import numpy as np
import re


class StrVariationObj:
    """
    """
    
    def __init__(self, var=[]):
        self.var = var
    
    def __repr__(self):
        return '{} {}'.format(self.__class__.__name__, self.var)
    
    def append(self, var):
        self.start.append(var)
     
    def varobjs2arr(self):
        """
        """
        return self.var


class NumVariationObj:
    """
    """
    
    def __init__(self, start='', *optargs):

        optargs = np.array([float(o) for o in optargs])
        self.start = np.array(float(start))
#       print(self.start)
        self.stop = np.array(float(start))
        self.incr = np.array(float(1))

        if len(optargs) == 1:
            self.stop = optargs[0]
        elif len(optargs) == 2:
            self.stop = optargs[1]
            self.incr = optargs[0]
    
    
    def __repr__(self):
        return '[{} [{}:{}:{}]]'.format(self.__class__.__name__,
            self.start, self.incr, self.stop,
        )
    
    
    def varobjs2arr(self):
        """
        """
        num = int(abs((self.stop-self.start) / self.incr) + 1)
        return np.linspace(self.start,self.stop,num)



class SimInputLang:
    """Class that defines a "Simumaltion Input Language".
    """
    file_actions = ['cpy', 'gen', 'ln']
    
    def __init__(self):
        pass




def is_numeric_var(strg):
    """
    """
    search=re.compile(r'[^0-9:.+-]').search
    return not bool(search(strg))


def parse_array(value):
    """Parses value and processes syntax (arrays and parameter variation).
    Returns array 
    
    Arguments:
    value -- string to be parsed
    
    Array syntax:
    [] -- delimits array input
    ,  -- separates columns of an array
    :  -- denotes range of values in MATLAB syntax:
          1:5   -> (1, 2, 3, 4, 5)
          1:2:5 -> (1, 3, 5)
    
    Examples:
    [1, 2, 3]       -> numpy array [1, 2, 3]
    1:3             -> numpy array [1, 2, 3]
    [1:3, 1:2:3]    -> list of numpy arrays [[1, 2, 3], [1, 3]]

    NOTE: Only one-dimensional arrays are currently supported. 
    """
    
    out = [] 
    
    # handle array column syntax
    value = value.strip(' ').strip('[').strip(']')
    value = value.split(',')
    value = [it.strip(' ') for it in value]
    
    nval = len(value)
#        print(value)
    # handle range syntax
    for it in value:
        
#        print(is_numerical_input(it), it)
        if is_numeric_var(it):
            it = it.split(':')
            if len(it) == 1:
                out.append(np.array(float(it[0])))
            else:
                out.append(NumVariationObj(*it))
        else:
            it = it.split(':')
            if len(it) == 1:
                out.append(it[0])
            else:
                print('Parameter variation syntax not supported for strings.')
                sys.exit()
#        print('parse_array():', out)
    return out




