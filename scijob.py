import sys # for exiting on errors
import numpy as np
import xml.dom.minidom
import importlib
from cmdobjs import CommandList, CommandObj
import itertools
import siminput
from fileobjs import FileList, FileObj
import simmods
from multiprocessing.pool import Pool
import time
import copy




def worker_run_sim(sim):
    """
    """
    sim.run()
    return

def worker_test(num):
    """
    """
    for i in range(9):
        print('Worker:', num)
        time.sleep(1)



class ParamVariation:
    """
    """
    def __init__(self, name=[], var=[]):
       self.name = name
       self.var = var
    
    
    def __repr__(self):
        return '[{}, {}]'.format(self.name, self.var)
    
    
    def var2arr(self):
        """
        """
        out = list(itertools.product(*self.var))
        return [list(a) for a in out]

    
    def append(self, name, array):
        """
        """
        assert isinstance(self.name, list), 'self.name is not a list'
        assert isinstance(self.var, list), 'self.var is not a list'
        self.name.append(name)
        self.var.append(array)





class SimParameterObj:
    """
    """
    def __init__(self, name='', val=[]):
       self.name = name
       self.val = val
    
    
    def __repr__(self):
        return '[{}, {}]'.format(self.name, self.val)
    
    
    def var2val(self):
        """
        """
        for val in self.val:
            if isinstance(val, siminput.NumVariationObj):
                tmp = val.varobjs2arr()
                try:
                    for el in np.nditer(tmp):
                        out_c = list(out)
#                        print('out_c:', out_c)
                        for o in out_c:
                            o.append(el)
                        out.append(o)
#                    print('out:', out)
                except NameError:
                    out = [[el] for el in np.nditer(tmp)]
#                    print('out:', out)
            else:
                try:
                    for o in out:
                        o.append(val)
                except NameError:
                    out = [[val]]
        return out


def xml_el_atts_data(base_el, atts, tags):
    """Parse xml element of shape
    <base_el att1="str1" att2="str2" ...>
        <tag1>val1<tag1>
        <tag2>val2<tag2>
        ...
    </base_el>
    
    and return ( [str1, str2, ...],
                 [val1, val2, ...])
    """
    if isinstance(atts, list):
        attdata = []
        for att in atts:
            attdata.append(xml_el_att(base_el, att))
    else:
        attdata = xml_el_att(base_el, atts)
    
    if isinstance(tags, list):
        tagdata = []
        for tag in tags:
            tagdata.append(xml_el_data(base_el, tag))
    else:
        tagdata = xml_el_data(base_el, tags)
    return attdata, tagdata


def xml_tag_atts_data(base_el, atts, tags):
    """Parse xml element of shape
    
    <base_el>
        <tag1 att1="str11" att2="str12" ...>
            val1
        <tag1>
        <tag2 att1="str21" att2="str22" ...>
            val2
        <tag2>
        ...
    </base_el>
    
    and return ( [[str11, str12, ...], [str21, str22, ...], [...]],
                 [val1, val2, ...]) 
    """
    # not implemented!!!!
    raise NotImplementedError('xml_tag_atts_data() not yet implemented')
    
    if isinstance(tags, list):
        tagdata = []
        for tag in tags:
            tmptag = base_el.getElementsByTagName(tag)
            
            tagdata.append(xml_el_data(base_el, tag))
            
            if isinstance(atts, list):
                attdata = []
                for att in atts:
                    attdata.append(xml_el_att(base_el, att))
            else:
                attdata = xml_el_att(base_el, atts)
    else:
        tagdata = xml_el_data(base_el, tags)
    return attdata, tagdata



def xml_el_att_data(base_el, att, tag):
    """
    tag - xml tag (string)
    """
    tmp = base_el.getElementsByTagName(tag)[0]
    data = tmp.childNodes[0].data
    attval = xml_el_att(tmp, att)
    return attval, data



def xml_el_att(base_el, att):
    """
    tag - xml tag (string)
    """
    assert base_el.hasAttribute(att), 'A tag lacks its attribute'
    return base_el.getAttribute(att)
    


def xml_el_data(base_el, tag):
    """
    """
    tmp = base_el.getElementsByTagName(tag)[0]
    return siminput.parse_array(tmp.childNodes[0].data)


class SimJob:
    """Class to manage several  and manipulate objects that store settings of
    simulations of inelastic scattering in the transmission electron microscope. 
    """
    
    def  __init__(self, sim_module, sim_params=[]):
        """
        """
        self.sim_module = importlib.import_module('simmods.' + sim_module)
        self.sim_module_name = sim_module
        self.sim_class = self.sim_module.SimDef
        self.sim_params = []  # list of sim parameters
        self.commands = CommandList()  # database of command objects
        self.sim_files = FileList()
        self.input_lang = siminput.SimInputLang
        
        
        if isinstance(sim_params,str):
            self.read_input_from_xml(sim_params)
        elif isinstance(sim_params,list):
            self.sim_params = sim_params
        else:
            print('SimJobInputTypeError: sim_params is neither list nor string')
            sys.exit()
    
    
    def read_input_from_xml(self, inputfile):
        """Read XML input file and save to class attributes
        """
        
        # Open XML inputfile using minidom parser
        DOMTree = xml.dom.minidom.parse(inputfile)
        inputdata = DOMTree.documentElement
        print(inputdata.getAttribute('sim'), self.sim_module_name)
        assert inputdata.getAttribute('sim') == self.sim_module_name, \
            '"simulation" attribute of input tag in input file ' \
            'does not match simulation attribute of JobSim instance'
        

        # Retrieve settings from XML input
        params = inputdata.getElementsByTagName('simparameters')[0]
        settings = params.getElementsByTagName('setting')
        for setting in settings:
            name, value = xml_el_atts_data(setting, 'name', 'value')
            self.sim_params.append(SimParameterObj(name, value))
#        print('self.sim_params:', self.sim_params)
        
        # Retrieve files from XML input
        files = inputdata.getElementsByTagName('file')
        file_actions = self.input_lang.file_actions
        for fi in files:
            action, path = xml_el_atts_data(fi, 'action', 'path')
            assert action in file_actions, 'a file action is not supported'
            self.sim_files.append_file(FileObj(path[0].split('/')[-1], path[0], action))
#        print('self.sim_files:', self.sim_files.as_array())
        
        # Retrieve execution order
        exec_order = inputdata.getElementsByTagName('execute')[0]
        self.commands.set_order(xml_el_data(exec_order, 'order'))
#        print('self.exec_order:', self.exec_order)
        
        # Retrieve programs and their arguments
        progs = inputdata.getElementsByTagName('program')
        for prog in progs:
            
            prog_id = xml_el_att(prog, 'id')
            prog_flag, prog_exec = xml_el_att_data(prog, 'flag', 'executable')
#            if prog_flag == 'cpy':
#                self.sim_files.append_file(FileObj(prog_exec, prog_flag))

            # Positional arguments
            posargs_tmp = []
            prog_posargs = prog.getElementsByTagName('posarg')
            for posarg in prog_posargs:
                posargs_tmp.append(
                    [xml_el_att(posarg, 'key'), xml_el_data(posarg, 'value')]
                    )
#            print('posargs_tmp:', posargs_tmp)
            # Arguments identified by keys or command line switches 
            args_tmp = []
            prog_args = prog.getElementsByTagName('arg')
            for arg in prog_args:
                args_tmp.append(
                    [xml_el_att(arg, 'key'), xml_el_data(arg, 'value')]
                    )
           
            # Assemble command list
            cmd_obj = CommandObj(
                exe=prog_exec, args=args_tmp, posargs=posargs_tmp,
                flag=prog_flag
            )
#            print(cmd_obj)
#            print(cmd_obj.gen_command())
            self.commands.append_cmd(key=prog_id, cmdobj=cmd_obj)
#            print(self.commands)
    
    
    
    def set_up_sims(self):
        """Resolve variations and generate simulation objects
        """
        sim = self.sim_class()
        valid_att = sim.attributes()
#        print(valid_att) 
        sim.exec_cmd = self.commands
        sim.file_list = self.sim_files
        
        var = ParamVariation()
        for param in self.sim_params:
#            print(param.name)
            assert param.name in valid_att, 'A parameter is not recognized'
            tmp = param.var2val()
#            print('param.name:', param.name, len(tmp), tmp)
            if len(tmp) == 1:
                setattr(sim,param.name,tmp[0])
            else:
                var.append(param.name, tmp)
#        print(var)
#        print('var.name:', var.name)
#        print('var.var:', var.var)
        tmp = var.var2arr()
#        print(tmp)
        self.sims_to_run = []
        for el in tmp:
            new_sim = copy.deepcopy(sim)
            rundir = []
            for name,val in zip(var.name,el):
#                print(name)
                setattr(new_sim,name,val)
#                print(val)
                rundir = rundir + [name] + [np.array_str(it) for it in val]

            new_sim.set_rundir('_'.join(rundir))
#            print('_'.join(run_dir))
            self.sims_to_run.append(new_sim)
        print('Job requires to perform %i simulation runs' % len(self.sims_to_run))
    
    
    
    def run_sims(self, nthreads=1):
        """Run simulations defined by the 
        """
        self.set_up_sims()
        if nthreads == 1:
            for sim in self.sims_to_run:
                sim.run()
        else:
            pool = Pool(processes=nthreads)
#           pool.map(worker_test, range(10))
            pool.map(worker_run_sim, self.sims_to_run)


