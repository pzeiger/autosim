#!/usr/bin/python3

import sys # for exiting on errors
import argparse # easy command line args
import scijob as scj
import numpy as np


def parse_cmd_args(argv=None):
    """Parse command line arguments using argparse
    """
    
    # Set up command line arg parser
    if argv is None:
        argv = sys.argv
        parser = argparse.ArgumentParser(
            prog=argv[0], description='Script to automate computation '
            'of inelastic electron scattering diffraction patterns.',
        )
        argv=argv[1:]
    else:
        parser = argparse.ArgumentParser(
            description='Script to automate computation of inelastic '
            'electron scattering diffraction patterns.',
        )
    parser.add_argument(
        'sim_module', nargs=1, help='name of simulation module to be used',
    )
    parser.add_argument(
        '-i', '--inputfile', default='auto_sim_input.xml', help='input file',
    )
    parser.add_argument(
        '-n', '--nthreads', default=1, help='number of threads to use',
        type=int,
    )
    parser.add_argument(
        '-v', '--version', action='version', version='%(prog)s 0.1',
    )
    
    # Get and return command line options
    return parser.parse_args(argv)


def main(argv=None):
    """Wrapper function representing the actual program.
    """
    
    # Get and print command line options
    args = parse_cmd_args()
    
    print('Executing simulation specified in module %s ...' \
        % (args.sim_module[0] )
    )
    print('Using input file', args.inputfile)
    print('Running on %i thread(s).' % args.nthreads)
    
    # Create new job instance with default settings and
    job = scj.SimJob(sim_module=args.sim_module[0], sim_params=args.inputfile)
    job.run_sims(nthreads=args.nthreads)
    
    
# run main if not imported
if __name__ == '__main__':
    sys.exit(main())


